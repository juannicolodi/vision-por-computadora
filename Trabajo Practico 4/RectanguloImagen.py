import cv2
import numpy as np
import sys

blue = (255,0,0) ; green = (0,255,0) ; red = (0,0,255)
drawing = False #
xybutton_down = -1,-1
xybutton_up = -1,-1
g=0

def dibuja(event,x,y,flags,param):

    global xybutton_down,xybutton_up,drawing,g,img

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        xybutton_down = x,y
    
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            img = np.array(imgcopia)
            xybutton_up = x,y
            cv2.rectangle(img,xybutton_down,(x,y),green,2)           
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        g=1


img = cv2.imread('hoja.jpg')
imgcopia = np.array(img)

cv2.namedWindow ('image')
cv2.setMouseCallback('image',dibuja)

while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):
        break
    if g == 1:
        if(k==ord('g')):
            img_g = img [ xybutton_down[1] : xybutton_up[1] , xybutton_down[0] : xybutton_up[0] ]
            cv2.imwrite('recorte.jpg',img_g)
            print('Imagen Guardada')
            sys.exit(0)
    if(k==ord('r')):
        img = np.array(imgcopia)
        g=0
        print('Imagen restaurada')

            
        
cv2.destroyAllWindows()
