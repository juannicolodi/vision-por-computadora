import cv2
import numpy as np

#----------------TP5-------------------
def euclideana(imagen,angulo,tx,ty,centro=None):

    h,w= imagen.shape[0],imagen.shape[1]

    if centro is None:
        centro = (w/2 , h/2 )
    
    M = cv2.getRotationMatrix2D(centro, angulo ,1) #Obtenemos la matriz
    
    M[0][2]+=tx #Sumamos la traslación
    M[1][2]+=ty
    
    img_rt = cv2.warpAffine(imagen, M, (w,h)) 
    
    cv2.imwrite('TransfEuclideana.jpg',img_rt)
    
    cv2.imshow('Recorte',imagen)
    cv2.imshow('Transformacion euclideana',img_rt)
    print('Presione cualquier tecla para continuar')
    cv2.waitKey(0)
    cv2.destroyWindow('Recorte')
    cv2.destroyWindow('Transformacion euclideana')


#------------------TP6-----------------
def similaridad(imagen,angulo,tx,ty,s,centro=None):

    h,w= imagen.shape[0],imagen.shape[1]

    if centro is None:
        centro = (w/2 , h/2 )
    
    M = cv2.getRotationMatrix2D(centro, angulo ,s) #Obtenemos la matriz
    
    M[0][2]+=tx #Sumamos la traslación
    M[1][2]+=ty
    
    img_s = cv2.warpAffine(imagen, M, (w,h)) 
    
    cv2.imwrite('TransfSimilaridad.jpg',img_s)

    cv2.imshow('Recorte',imagen)
    cv2.imshow('Transformacion Similaridad',img_s)
    print('Presione cualquier tecla para continuar')
    cv2.waitKey(0)
    cv2.destroyWindow('Recorte')
    cv2.destroyWindow('Transformacion Similaridad')
    

#----------------TP7------------------
def afin(imagen):
    
    blue = (255,0,0)
    ax=[]
    ay=[]
    
    def img(event,x,y,flags,param):
        
        
        if(event==cv2.EVENT_LBUTTONDOWN and len(ax)<3):

            cv2.line(imagen,(x,y),(x,y),blue,5)
            ax.append(x)
            ay.append(y)
            #print(ax , ' , ' ,ay)
        
        elif(event==cv2.EVENT_LBUTTONUP and len(ax)==3):
            
            scr = cv2.imread('perro.png')
            
            srcTri = np.float32([[0, 0], [scr.shape[1], 0], [scr.shape[1], scr.shape[0]]])#Puntos de la imagen origen
            dstTri = np.float32([[ax[0],ay[0]],[ax[1],ay[1]],[ax[2],ay[2]]])#Puntos destinos

            warp_mat = cv2.getAffineTransform(srcTri, dstTri)

            warp_dst = cv2.warpAffine(scr, warp_mat, (imagen.shape[1], imagen.shape[0]))

            masca= np.zeros([imagen.shape[0],imagen.shape[1],imagen.shape[2]],np.uint8)

            for i,row in enumerate(warp_dst):
                for j,col in enumerate(row):
                    if(col[0]==0 and col[1]==0 and col[2]==0):
                        masca[i,j,:] = 1

            imgAux = (masca) * imagen

            imagenTotal=  imgAux + warp_dst

            cv2.imwrite('TransfAfin.jpg',imagenTotal)

            cv2.imshow('ImagenModificada',imagenTotal)
            print('Presione cualquier tecla para continuar')
            cv2.waitKey(0)
            cv2.destroyWindow('ImagenModificada')
            print("Presione 'v' para volver al programa principal")
            
        

    cv2.namedWindow ('imagenAfin')
    cv2.setMouseCallback('imagenAfin',img)

    while(True):
    
        cv2.imshow('imagenAfin',imagen)
        
        k = cv2.waitKey(1) & 0xFF
    
        if(k == ord('v')):#Volver
            cv2.destroyWindow('imagenAfin')
            break
